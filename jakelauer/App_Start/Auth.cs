﻿using System;
using System.Threading.Tasks;
using System.Web;
using RedditSharp;

namespace jakelauer
{
	public class Auth
	{
		public const bool SecureMode = false;
		public const string AccessTokenCookieId = "access_token";
		public const string AccessTokenExpireId = "access_expiration";
		private const string TimeFormat = "yyyyMMddHHmmss";
		public const int CookieExpireDays = 7;

		public Auth()
		{
		}

		public static TokenCookieSet GetAccessTokenCookieSet(HttpRequestBase request)
		{
			var accessTokenCookie = request.Cookies[Auth.AccessTokenCookieId];
			var accessTokenExpireCookie = request.Cookies[Auth.AccessTokenExpireId];
			if (accessTokenCookie != null && accessTokenExpireCookie != null)
			{
				return new TokenCookieSet(accessTokenCookie, accessTokenExpireCookie);	
			}
			return null;
		}

		public static TokenCookieSet StoreAccessTokenCookie(string accessToken, HttpResponseBase response)
		{
			var expiresDays = Auth.CookieExpireDays;

			var tokenCookie = new HttpCookie(AccessTokenCookieId, accessToken) { Secure = SecureMode };
			var tokenExpireCookie = new HttpCookie(AccessTokenExpireId, DateTime.UtcNow.AddMinutes(50).ToString(TimeFormat));

			// If we set expiresDays to 0, we just want a sessionCookie
			if (expiresDays != 0)
			{
				tokenCookie.Expires = DateTime.UtcNow.AddDays(expiresDays);
				tokenExpireCookie.Expires = tokenCookie.Expires;
			}

			response.Cookies.Set(tokenCookie);
			response.Cookies.Set(tokenExpireCookie);

			var tokenCookieSet = new TokenCookieSet(tokenCookie, tokenExpireCookie);

			return tokenCookieSet;
		}

		public static void ClearCookie(HttpResponseBase response, string cookieName)
		{
			var httpCookie = response.Cookies[cookieName];
			if (httpCookie != null)
			{
				httpCookie.Expires = DateTime.UtcNow.AddDays(-1);
			}
		}

		public static bool IsAuthenticated(HttpRequestBase request)
		{
			var accessTokenCookieSet = GetAccessTokenCookieSet(request);
			var hasCookies = accessTokenCookieSet != null;
			return hasCookies;
		}

		public static bool NeedsRefresh(HttpRequestBase request)
		{
			var accessTokenCookieSet = GetAccessTokenCookieSet(request);
			var hasCookies = accessTokenCookieSet != null;
			if (hasCookies)
			{
				var date = DateTime.ParseExact(accessTokenCookieSet.TokenExpirationCookie.Value, TimeFormat, null);
				if (date < DateTime.UtcNow)
				{
					return true;
				}
			}

			return false;
		}

		public static string GetAccessToken(HttpRequestBase request)
		{
			var cookieSet = GetAccessTokenCookieSet(request);
			return cookieSet.TokenCookie.Value;
		}
	}

	public class TokenCookieSet
	{
		public HttpCookie TokenCookie { get; set; }
		public HttpCookie TokenExpirationCookie { get; set; }
		public TokenCookieSet(HttpCookie tokenCookie, HttpCookie tokenExpirationCookie)
		{
			TokenCookie = tokenCookie;
			TokenExpirationCookie = tokenExpirationCookie;
		}
	}
}