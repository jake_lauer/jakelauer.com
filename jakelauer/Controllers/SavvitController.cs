﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using jakelauer.App_Start;
using jakelauer.Models;
using jakelauer.Models.Savvit;
using RedditSharp;

namespace jakelauer.Controllers
{
	public class SavvitController : Controller
	{
		[OutputCache(Duration=20)]
		public ActionResult Index(int page = 0)
		{
			var isAuthed = Auth.IsAuthenticated(Request);
			if (!isAuthed)
			{
				return RedirectToAction("Authorize");
			}
			else
			{
				var needsRefresh = Auth.NeedsRefresh(Request);
				if (needsRefresh)
				{
					var accessTokenSet = Auth.GetAccessTokenCookieSet(Request);
					var tokenCookieCode = accessTokenSet.TokenCookie.Value;
					AttemptAuth(tokenCookieCode, true);
				}
			}

			var model = new SavvitModel(Request, page);
			return View(model);
		}

		public ActionResult Authorize()
		{
			var model = new SavvitAuthModel();
			model.GetAuthString();
			return View(model);
		}

		public ActionResult AuthReturn(string state, string code)
		{
			this.AttemptAuth(code, false);
		    return View();
		}

		private TokenCookieSet AttemptAuth(string code, bool isRefresh)
		{
			var authProvider = new AuthProvider(AuthConfig.ClientId, AuthConfig.ClientSecret, AuthConfig.RedirectUrl);
			var accessToken = authProvider.GetOAuthToken(code, false);
			var tokenCookieSet = Auth.StoreAccessTokenCookie(accessToken, Response);

			return tokenCookieSet;
		}
	}
}
