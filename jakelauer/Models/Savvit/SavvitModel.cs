﻿using RedditSharp;
using RedditSharp.Things;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace jakelauer.Models.Savvit
{
	public class SavvitModel
	{
		public string AccessToken { get; set; }
		public IEnumerable<Thing> Saved { get; set; }
		public int Page { get; set; }

		private int PerPage = 1;

		public SavvitModel(HttpRequestBase request, int page = 0)
		{
			Page = page;

			AccessToken = Auth.GetAccessToken(request);
			var reddit = new Reddit(AccessToken);
			reddit.InitOrUpdateUser();

			var skip = page * PerPage;
			Saved = reddit.User.SavedThings.Skip(skip).Take(PerPage);
		}
	}
}