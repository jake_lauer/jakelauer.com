﻿using System.Linq;
using System.Web;
using jakelauer.App_Start;
using RedditSharp;
using RedditSharp.Things;

namespace jakelauer.Models.Savvit
{
	public class SavvitAuthModel
	{
		private AuthProvider AuthProvider { get; set; }
		public string AuthUrl { get; set; }

		public SavvitAuthModel()
		{
			this.AuthProvider = new AuthProvider(AuthConfig.ClientId, AuthConfig.ClientSecret, AuthConfig.RedirectUrl);
		}

		public void GetAuthString()
		{
			var scope = AuthProvider.Scope.identity 
				| AuthProvider.Scope.save 
				| AuthProvider.Scope.history
				| AuthProvider.Scope.read;
			this.AuthUrl = this.AuthProvider.GetAuthUrl("makestatestring", scope, true);
		}

		public void OnAuthReturn(string state, string code, HttpResponseBase response)
		{
			var accessToken = AuthProvider.GetOAuthToken(code, false);
			Auth.StoreAccessTokenCookie(accessToken, response);
		}
	}
}