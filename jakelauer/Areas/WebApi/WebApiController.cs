﻿using System.Web.Mvc;

namespace jakelauer.Areas.WebApi
{
	public class WebApiController : Controller
	{
		public string AccessToken { get; set; }

		public ActionResult Index()
		{
			return View();
		}

		public ActionResult ApiViewDefault(object data)
		{
			object modelData = Json(data);

			return View("Default", modelData);
		}

		public ActionResult ToggleCommentSave(string subreddit, string name, string linkName)
		{
			var redditAccess = new RedditAccess(Request);
			var saved = redditAccess.ToggleCommentSave(subreddit, name, linkName);

			return ApiViewDefault(new { response = saved });
		}

		//[Route("WebApi/Like/{postId}/{reblogKey}")]
		//public async Task<ActionResult> Like(long postId, string reblogKey)
		//{
		//	var tumblrApiClient = new TumblrApi(Request);
		//	await tumblrApiClient.Client.LikeAsync(postId, reblogKey);

		//	return ApiViewDefault(new { response = true });
		//}

	}
}
