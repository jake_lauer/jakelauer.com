﻿using RedditSharp;
using RedditSharp.Things;
using System;
using System.Web;

namespace jakelauer.Areas.WebApi
{
	public class RedditAccess
	{
		private Reddit Reddit { get; set; }

		public RedditAccess(HttpRequestBase request)
		{
			var accessToken = Auth.GetAccessToken(request);
			Reddit = new Reddit(accessToken);
			Reddit.InitOrUpdateUser();
		}

		public bool ToggleCommentSave(string subreddit, string name, string linkName)
		{
			var comment = Reddit.GetComment(subreddit, name, linkName);

			return ToggleThingSave(comment);
		}

		public bool TogglePostSave(Uri uri)
		{
			var post = Reddit.GetPost(uri);

			return ToggleThingSave(post);
		}

		private bool ToggleThingSave(VotableThing thing)
		{
			if (thing.Saved)
			{
				thing.Unsave();
			}
			else
			{
				thing.Save();
			}

			return true;
		}
	}
}