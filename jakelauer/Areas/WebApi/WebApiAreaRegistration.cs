﻿using System.Web.Mvc;

namespace jakelauer.Areas.WebApi
{
	public class WebApiAreaRegistration : AreaRegistration 
	{
		public override string AreaName 
		{
			get 
			{
				return "WebApi";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute(
				"WebApi_default",
				"WebApi/{action}",
				new { controller = "WebApi", action = "Index", id = UrlParameter.Optional },
				new[] { "jakelauer.Areas.WebApi" }
			);
		}
	}
}